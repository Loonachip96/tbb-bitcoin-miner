#include "StaticVars.h"

bool         Static::doExit             = false;
BigNumber    Static::headBlockID        = 0;
Digest       Static::headBlockDigest    = Digest{"11112222333344445555666677778888"};
BlockChain   Static::blockChain         = {"initial block chain data nanananana"};

TransactionList Static::transactions;
std::mutex      Static::transactionUptadeMutex;
std::mutex      Static::blockUptadeMutex;


BigNumber Static::getHeadID(){
    return Static::headBlockID;
}

BigNumber Static::getHeadDigest(){
    return Static::headBlockDigest.asBigNumber();
}

int Static::getChainLength(){
    return Static::blockChain.size();
}

void Static::pushTransaction(std::string t){
    transactionUptadeMutex.lock();
    transactions.push_back(t);
    transactionUptadeMutex.unlock();
}

std::string Static::getTransaction(int i){
    if(Static::transactions.size() <= i)
        return "";

    auto iterator = Static::transactions.begin();
    std::advance(iterator , i);

    return *iterator;
}

void Static::pushNewBlock(std::string blockData, BigNumber newBlockDigest){
    blockUptadeMutex.lock();
    
    Static::blockChain.push_back(blockData);
    Static::headBlockDigest = newBlockDigest;
    Static::headBlockID++;

    blockUptadeMutex.unlock();
}