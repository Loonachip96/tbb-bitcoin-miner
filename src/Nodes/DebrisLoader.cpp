#include "Nodes/DebrisLoader.h"
#include "Miner.h"
#include "AtomicPrint.h"

namespace{
    using Node_t = DebrisLoaderFactory::Node_t;
    using NodeData_t = DebrisLoaderFactory::NodeData_t;
    using NodeInstance_t = DebrisLoaderFactory::NodeInstance_t;
}

auto DebrisLoaderFunctional::operator()(
    TaggedDebris taggedDebris
) -> TaggedDebrisCart {
    auto & transaction = std::get<0>(taggedDebris);
    auto & tag = std::get<1>(taggedDebris);

    Atomic::cout{} << "[DebrisLoader]<" << this->ID << "> loading:\t" << transaction << Atomic::cout::endlAndFlush;

    auto & cart = std::get<0>(this->cargoCart);
    auto & cartTag = std::get<1>(this->cargoCart);
    cart.push_back(transaction);
    cartTag = tag;

    if(cart.size() >= Miner::TRANSACTIONS_PER_TREE){
        
        TaggedDebrisCart taggedBucket;
        auto & bucket = std::get<0>(taggedBucket);
        std::get<1>(taggedBucket) = cartTag;

        for (size_t shovel = 0; shovel < Miner::TRANSACTIONS_PER_TREE; shovel++){
            bucket.push_back( *(cart.begin()) );
            cart.pop_front();
        }

        Atomic::cout{} << "[DebrisLoader]<" << this->ID << ">\t\tsending full cargo cart" << Atomic::cout::endlAndFlush;
        return taggedBucket;

    }else
        return TaggedDebrisCart{TransactionList{}, tag};
}

NodeInstance_t DebrisLoaderFactory::Create(int objectID){
    NodeInstance_t n;

    n.data = std::make_shared<NodeData_t>();
    n.node = std::make_shared<Node_t>( 
        *_graphPtr, 
        tbb::flow::unlimited,
        DebrisLoaderFunctional{objectID}
    );

    return n;
}

