#include "Nodes/Miner.h"
#include "Timer.h"
#include "AtomicPrint.h"

namespace{
    using Node_t = MinerFactory::Node_t;
    using NodeData_t = MinerFactory::NodeData_t;
    using NodeInstance_t = MinerFactory::NodeInstance_t;
}

auto MinerFunctional::operator()(
    TaggedDebrisCart taggedCart
) -> MineralsFound{
    auto & debrisBucket = std::get<0>(taggedCart);
    auto & tag = std::get<1>(taggedCart);

    bool newDebrisBucketPending = debrisBucket.size() != 0;
    bool blockAlreadyMined = 
        ! tag.headBlockID.getString().empty() 
        && ! tag.headBlockID.equals( this->cachedHeadID );

    if(blockAlreadyMined){
        if(! this->finishedPreviousJob){
            Atomic::cout{} << "[Miner]<" << this->ID << ">\t\t\tabandoning work" << Atomic::cout::endlAndFlush;
            this->m.abandonWork();
        }
        this->cachedHeadID = Static::getHeadID();
    }

    if(newDebrisBucketPending){
        this->debrisBucketQueue.push(debrisBucket);
    }

    BusyPollTimer<bool>{std::chrono::milliseconds{100}, false, 
        [&]() -> bool{ 
            blockAlreadyMined = ! this->cachedHeadID.equals( Static::getHeadID() );

            if(blockAlreadyMined)
                this->m.abandonWork();
            
            return this->m.isBusy(); 
        }
    };

    if(this->debrisBucketQueue.size() > 0){
        this->finishedPreviousJob = false;
        this->m.feed(tag.headBlockDigest, this->cachedHeadID, this->debrisBucketQueue.front(), tag.demandedPoWLevel );
        this->debrisBucketQueue.pop();     
    }
    else{
        return std::pair<BlockHeader, BigNumber>{{}, ""};
    }

    auto minedBlock = this->m.mine();

    if(!m.isBusy() && minedBlock.second.getString().size() > 0){
        Atomic::cout{} << "[Miner]<" << this->ID << ">\t\t\tHAS MINED NEW BLOCK!!!" << Atomic::cout::endlAndFlush;
        this->finishedPreviousJob = true;
        //Miner::bitDump(minedBlock.second);
    }
    return minedBlock;
}

NodeInstance_t MinerFactory::Create(int objectID){
    NodeInstance_t n;

    n.data = std::make_shared<NodeData_t>();
    n.node = std::make_shared<Node_t>( 
        *_graphPtr, 
        tbb::flow::unlimited,
        MinerFunctional{objectID}
    );

    return n;
}

