#include "Nodes/CargoConveyor.h"

namespace{
    using Node_t = CargoConveyorFactory::Node_t;
    using NodeData_t = CargoConveyorFactory::NodeData_t;
    using NodeInstance_t = CargoConveyorFactory::NodeInstance_t;
}

NodeInstance_t CargoConveyorFactory::Create(int objectID){
    NodeInstance_t n;

    n.data = std::make_shared<NodeData_t>();
    n.node = std::make_shared<Node_t>(*_graphPtr);

    return n;
}

