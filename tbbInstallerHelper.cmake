include(${CMAKE_CURRENT_LIST_DIR}/externalsHelper.cmake)

function(InstallTBB headersInstallPath libsInstallPath)
    set(TBB_CMAKE_DIR "${CMAKE_CURRENT_BINARY_DIR}/TBB")
    set(TBB_REPO_DIR "${TBB_CMAKE_DIR}/src/TBBExternal")
    set(TBB_CMAKE "${TBB_CMAKE_DIR}/CMakeLists.txt")

    file(WRITE "${TBB_CMAKE}"   "cmake_minimum_required(VERSION 3.10)\n")
    file(APPEND "${TBB_CMAKE}"  "\n")
    file(APPEND "${TBB_CMAKE}"  "include(${TBB_REPO_DIR}/cmake/TBBBuild.cmake)\n")
    file(APPEND "${TBB_CMAKE}"  "\n")
    file(APPEND "${TBB_CMAKE}"  "tbb_build(\n")
    file(APPEND "${TBB_CMAKE}"  "    TBB_ROOT ${TBB_REPO_DIR} \n")
    file(APPEND "${TBB_CMAKE}"  "    CONFIG_DIR ${TBB_REPO_DIR} \n")
    file(APPEND "${TBB_CMAKE}"  ")\n")
    file(APPEND "${TBB_CMAKE}"  "\n")
    file(APPEND "${TBB_CMAKE}"  "file(\n")
    file(APPEND "${TBB_CMAKE}"  "   COPY ${TBB_REPO_DIR}/include/tbb/\n")
    file(APPEND "${TBB_CMAKE}"  "   DESTINATION ${headersInstallPath}/TBB\n")
    file(APPEND "${TBB_CMAKE}"  "   PATTERN \"*\"\n")
    file(APPEND "${TBB_CMAKE}"  ")\n")
    file(APPEND "${TBB_CMAKE}"  "file(\n")
    file(APPEND "${TBB_CMAKE}"  "   COPY ${TBB_REPO_DIR}/include/serial\n")
    file(APPEND "${TBB_CMAKE}"  "   DESTINATION ${headersInstallPath}/TBB\n")
    file(APPEND "${TBB_CMAKE}"  ")\n")
    file(APPEND "${TBB_CMAKE}"  "file(\n")
    file(APPEND "${TBB_CMAKE}"  "   COPY ${TBB_REPO_DIR}/include/tbb/\n")
    file(APPEND "${TBB_CMAKE}"  "   DESTINATION ${headersInstallPath}/TBB\n")
    file(APPEND "${TBB_CMAKE}"  "   PATTERN \"*\"\n")
    file(APPEND "${TBB_CMAKE}"  ")\n")
    file(APPEND "${TBB_CMAKE}"  "file(\n")
    file(APPEND "${TBB_CMAKE}"  "   COPY ${TBB_CMAKE_DIR}/tbb_cmake_build/tbb_cmake_build_subdir_release/\n")
    file(APPEND "${TBB_CMAKE}"  "   DESTINATION ${libsInstallPath}\n")
    file(APPEND "${TBB_CMAKE}"  "   FILES_MATCHING PATTERN \"*.so\"\n")
    file(APPEND "${TBB_CMAKE}"  ")\n")

    CustomAddAndInstallFromGit(
        PROJ_NAME "TBB"
        GIT_LINK "https://github.com/intel/tbb.git"
        CONFIGURE_COMMAND git checkout tbb_2020 && cd ${TBB_CMAKE_DIR} && cmake . -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        CONFIGURE_CWD ${TBB_REPO_DIR}
    )
endfunction()