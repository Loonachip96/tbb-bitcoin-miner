#pragma once

#include "BlockHeader.h"
#include "Digest.h"
#include "TypeDefs.h"
#include <BigNumber/bignumber.h>
#include <utility>
#include <list>
#include <random>

class Miner{
public:
    Miner();
    ~Miner();
    constexpr static const int TRANSACTIONS_PER_TREE = 4;

    static auto bitDump(BigNumber number) -> void;

    auto feed(
        Digest previousBlockHash,
        BigNumber previousBlockID,
        std::list<std::string> newTransactions, 
        size_t PoWZerosAmount
    ) -> void;

    auto mine() -> MineralsFound;
    auto abandonWork() -> void;
    auto isBusy() -> bool;

private:
    auto nBitOf(size_t nBit, Digest& number) -> int;
    auto getMerkleDigest() -> BigNumber;
    auto areNLeftBitsZeros(size_t n, Digest& digest) -> bool;

private:
    size_t PoWzerosCount{0};
    std::list<std::string> transactions; 
    BlockHeader blockBeingMined;
    bool shouldAbandonWork = false;
    bool isMining = false;
    std::mt19937 randomEngine{std::random_device{}()};
};
