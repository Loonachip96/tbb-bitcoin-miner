#pragma once

#include "Digest.h"
#include "BlockHeader.h"
#include <TBB/flow_graph.h>
#include <string>
#include <list>

using NodeInfoTag_t = struct{
    int instanceID{-1};
};

class DebrisCartTag{
public:
    DebrisCartTag() = default;
    DebrisCartTag(
        BigNumber inHeadBlockID,
        Digest inHeadBlockDigest,
        size_t inDemandedPoWLevel
    )
        : headBlockID{inHeadBlockID}
        , headBlockDigest{inHeadBlockDigest}
        , demandedPoWLevel{inDemandedPoWLevel}
    { }

    DebrisCartTag& operator=(DebrisCartTag const & inObj){
        this->demandedPoWLevel = inObj.demandedPoWLevel;
        this->headBlockDigest = Digest{inObj.headBlockDigest}.asBigNumber();
        this->headBlockID = inObj.headBlockID;
    }

    BigNumber headBlockID{""};
    Digest headBlockDigest{};
    size_t demandedPoWLevel{0};
};

using Transaction = std::string;
using TransactionList = std::list<Transaction>;
using TaggedDebris = tbb::flow::tuple<Transaction, DebrisCartTag>;
using TaggedDebrisCart = tbb::flow::tuple<TransactionList, DebrisCartTag>;
using MineralsFound = std::pair<BlockHeader, BigNumber>;

constexpr static const int NUM_ACTORS = 4;
using Haulier_t = tbb::flow::indexer_node<MineralsFound, MineralsFound, MineralsFound, MineralsFound>;

using IndexedMineralPack = Haulier_t::output_type;

using BlockChain = std::list<std::string>;