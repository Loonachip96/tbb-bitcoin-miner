#pragma once

#include "Node.h"
#include "Digest.h"
#include "TypeDefs.h"
#include <TBB/flow_graph.h>

class MineNodeFunctional{
private:
    const int ID;
    TaggedDebrisCart cargoCart;
    int generatedTransactionsCount = 0;
public:
    MineNodeFunctional(int objectID) : ID{objectID} { }
    auto operator()(TaggedDebris& taggedDebris) -> bool;
};

class MineNodeFactory : public NodeFactory< tbb::flow::source_node<TaggedDebris>, NodeInfoTag_t >{
public:
    MineNodeFactory(tbb::flow::graph* graphPtr) : NodeFactory(graphPtr) {}
    NodeInstance_t Create(int objectID) override;
};


class MineNode : public NodeArray< 1, MineNodeFactory::Node_t, MineNodeFactory::NodeData_t >
{
public:
    MineNode(tbb::flow::graph& g)
        : NodeArray< 1, MineNodeFactory::Node_t, MineNodeFactory::NodeData_t >( 
            std::make_shared<MineNodeFactory>(&g) 
        )
    { }
    
    ~MineNode() = default;
};