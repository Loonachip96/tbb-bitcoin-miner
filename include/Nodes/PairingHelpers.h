#pragma once

class MakeEdges{
public:
    template<typename TLNode, typename TRNodes>
    static void oneToAll(TLNode& l, TRNodes& r){
        static_assert(TLNode::capacity == 1, "Left operand has to have one element only");
        static_assert(TRNodes::capacity > 0, "Right operand has to have at least one element");

        for (size_t i = 0; i < TRNodes::capacity; i++){
            tbb::flow::make_edge(l.getNode(), r.getNode(i));
        }
    }

    template<typename TLNodes, typename TRNodes>
    static void parallel(TLNodes& l, TRNodes& r){
        static_assert(TLNodes::capacity == TRNodes::capacity, "Both operands have to have same elements count");

        for (size_t i = 0; i < TRNodes::capacity; i++){
            tbb::flow::make_edge(l.getNode(i), r.getNode(i));
        }
    }
};
