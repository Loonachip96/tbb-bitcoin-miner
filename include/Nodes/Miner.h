#pragma once

#include "../Miner.h"
#include "Node.h"
#include "TypeDefs.h"
#include "StaticVars.h"
#include <TBB/flow_graph.h>
#include <queue>

class MinerFunctional{
private:
    const int ID;
    Miner m;
    std::queue<TransactionList> debrisBucketQueue;
    BigNumber cachedHeadID;
    bool finishedPreviousJob = true;
public:
    MinerFunctional(int objectID) 
        : ID{objectID}
        , cachedHeadID{Static::getHeadID()} 
    { }
    auto operator()(TaggedDebrisCart taggedCart) -> MineralsFound;
};

class MinerFactory : public NodeFactory< tbb::flow::function_node<TaggedDebrisCart, MineralsFound>, NodeInfoTag_t >{
public:
    MinerFactory(tbb::flow::graph* graphPtr) : NodeFactory(graphPtr) {}
    NodeInstance_t Create(int objectID) override;
};

template<int TAmount>
class MinerNodes : public NodeArray< TAmount, MinerFactory::Node_t, MinerFactory::NodeData_t >
{
public:
    MinerNodes(tbb::flow::graph& g)
        : NodeArray< TAmount, MinerFactory::Node_t, MinerFactory::NodeData_t >( 
            std::make_shared<MinerFactory>(&g) 
        )
    { }
    ~MinerNodes() = default;
};