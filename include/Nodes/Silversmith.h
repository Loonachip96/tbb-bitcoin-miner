#pragma once

#include "Node.h"
#include "TypeDefs.h"
#include <TBB/flow_graph.h>
#include <mutex>

class SilversmithFunctional{
private:
    const int ID;
    static std::mutex m;
public:
    SilversmithFunctional(int objectID) : ID{objectID} { }
    auto operator()(const IndexedMineralPack& indexedMinerals) -> void;
};

class SilversmithFactory : public NodeFactory< tbb::flow::function_node<IndexedMineralPack>, NodeInfoTag_t >{
public:
    SilversmithFactory(tbb::flow::graph* graphPtr) : NodeFactory(graphPtr) {}
    NodeInstance_t Create(int objectID) override;
};


class SilversmithNode : public NodeArray< 1, SilversmithFactory::Node_t, SilversmithFactory::NodeData_t >
{
public:
    SilversmithNode(tbb::flow::graph& g)
        : NodeArray< 1, SilversmithFactory::Node_t, SilversmithFactory::NodeData_t >( 
            std::make_shared<SilversmithFactory>(&g) 
        )
    { }
    
    ~SilversmithNode() = default;
};