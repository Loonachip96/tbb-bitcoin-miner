#pragma once
#include "Miner.h"
#include "TypeDefs.h"
#include <mutex>

class Static{
public:
    static bool doExit;
    
    constexpr static const int NUM_TRANSACTIONS = Miner::TRANSACTIONS_PER_TREE * 6;
    constexpr static const int PoW_LEVEL = 10;


private:
    static BigNumber headBlockID;
    static Digest headBlockDigest;
    static BlockChain blockChain;

    static TransactionList transactions;
    static std::mutex transactionUptadeMutex;
    static std::mutex blockUptadeMutex;


public:
    static BigNumber getHeadID();
    static BigNumber getHeadDigest();
    static int getChainLength();

    static void pushTransaction(std::string t);
    static std::string getTransaction(int i);
    static void pushNewBlock(std::string blockData, BigNumber newBlockDigest);

};