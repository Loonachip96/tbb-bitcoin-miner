#pragma once

#include <CryptoCpp/config_int.h>
#include <BigNumber/bignumber.h>
#include <memory>
#include <string>

class DataBlob{
private:
    using Blob_t = CryptoPP::byte []; 
    using Blob_p = std::shared_ptr<Blob_t>; 

public:
    DataBlob();
    DataBlob(const DataBlob& inObj);
    DataBlob(DataBlob&& inObj);
    DataBlob(CryptoPP::byte* bytes, size_t bytesAmount);
    DataBlob(const char* data);
    ~DataBlob();

    auto operator[](int i) -> CryptoPP::byte&;
    auto operator=(DataBlob&& newBlob) -> DataBlob&;
    auto allocate(size_t size) -> void;

    auto size() -> size_t;
    auto toCryptoBytesPtr() -> CryptoPP::byte*;
    auto toBigNumber() -> BigNumber;
    auto toStdString() -> std::string;

private:
    Blob_p blobBytes;
    size_t blobSize;
};