#pragma once

#include <sstream>
#include <mutex>

namespace Atomic{

    class cout{
    private:
        std::string stream;

    public:
        template<typename T>
        inline cout& operator<<(T value){
            this->stream += value;
            return *this;
        }

        cout& operator<<(int value){
            this->stream += std::to_string( value );
            return *this;
        }

        cout& operator<<( void(distinguishFunction)(void) ){
            stream += '\n';
            return *this;
        }

        cout& operator<<( void(distinguishFunction)(int) ){
            std::printf( this->stream.c_str() );
            this->stream.clear();
            return *this;
        }

        cout& operator<<( void(distinguishFunction)(char) ){
            stream += '\n';
            std::printf( this->stream.c_str() );
            this->stream.clear();
            return *this;
        }

        static void endl(void){}
        static void flush(int){}
        static void endlAndFlush(char){}
    };

}